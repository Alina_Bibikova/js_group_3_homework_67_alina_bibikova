import React, { Component } from 'react';
import './App.css';
import {connect} from 'react-redux';

class App extends Component {
  render() {
      return (
          <div className='calculator'>
            <div className={'itemName '.concat(this.props.isRight)}>
                <h1>{this.props.ctr}</h1>
            </div>
            <div>
                <button onClick={this.props.addPassword}>1</button>
                <button onClick={this.props.addPassword}>2</button>
                <button onClick={this.props.addPassword}>3</button>
            </div>
            <div>
                <button onClick={this.props.addPassword}>4</button>
                <button onClick={this.props.addPassword}>5</button>
                <button onClick={this.props.addPassword}>6</button>
            </div>
            <div>
                <button onClick={this.props.addPassword}>7</button>
                <button onClick={this.props.addPassword}>8</button>
                <button onClick={this.props.addPassword}>9</button>
            </div>
            <div>
                <button onClick={this.props.delete}>D</button>
                <button onClick={this.props.addPassword}>0</button>
                <button onClick={this.props.enter}>E</button>
            </div>
          </div>
      );
  }
}

const mapStateToProps = state => {
    return {
        ctr: state.symbols,
        isRight: state.isRight
    }
};

const mapDispatchToProps = dispatch => {
    return {
        enter: () => dispatch({type: 'ENTER'}),
        delete: () => dispatch({type: 'DELETE'}),
        addPassword: (e) => dispatch({type: 'ADD', value: e.target.innerText}),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
