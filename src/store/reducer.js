const initialState = {
    writePassword: '1234',
    password: '',
    symbols: '',
    isRight: ''
};

const reducer = (state = initialState, action) => {

    if (action.type === 'DELETE') {
        return {
            ...state,
            password: state.password.slice(0, state.password.length -1),
            symbols: state.symbols.slice(0, state.symbols.length -1),
        };
    }

    if (action.type === 'ENTER') {
        if (state.password === state.writePassword){
            return {
                ...state,
                isRight: 'open',
                symbols: state.symbols = 'Access Granted'
            };
        } else return {
            ...state,
            isRight: 'danger',
            symbols: state.symbols = 'Danger'
        }
    }


    if (action.type === 'ADD') {
        const newState = {...state};

        if (state.isRight !== ''){
            newState.password = '';
            newState.symbols = '';
            newState.isRight = ''
        }

        if (newState.password.length < 4){
            newState.password = newState.password + action.value;
            newState.symbols = newState.symbols + '*';
        }
        return newState;
    }

    return state;
};

export default reducer;